/**
 * @description:This file entry point for node Application.Here all controllers will be loaded
 * @author: Rambabu
 * @date :20-09-2020
 */

const express = require("express");
const cookieParser = require("cookie-parser"),
  bodyParser = require("body-parser");
const path = require("path");
const compression = require("compression");
const cors = require("cors");
const mongoose = require("mongoose");

//Express is a minimal and flexible Node.js web application framework
const app = express();

//Starts the server here with desired port is 9000
//===================================
const server = require("http").createServer(app);
server.listen(process.env.PORT || 9000);
console.log(`Server running....${process.env.PORT || 9000}!`);

mongoose.connect("mongodb://localhost:27017/eicu", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});
mongoose.set("debug", true);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.once("open", function callback() {
  console.log("db connected...!");
});

// for parsing application/json
app.use(bodyParser.json());
// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
//Parse Cookie header and populate req.cookies with an object keyed by the cookie names.
app.use(cookieParser());
// Compres all the response Body
app.use(compression());
//Cross-Origin Resource Sharing
app.use(cors({ origin: "*" }));

const winston = require("winston");
// App settings
const { sillyLogConfig } = require("./helper/logger").winston;

// Create the logger
global.logger = winston.createLogger(sillyLogConfig);

app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With, content-type, Authorization"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});

//express.static serve static files, such as images, CSS, JavaScript, etc
app.use(express.static(path.join(__dirname, "public")));

/**
 * @description: Loading props based on enviornment
 * @author: Rambabu
 * @date :  20-09-2020
 */
require("dotenv").config({ path: __dirname + "/.env" + process.env.NODE_ENV });

console.log("eICU Db-Connector Environment is----" + process.env.environment);

/**
 * @description:Common Base router
 * @author: Rambabu K
 * @date :20-09-2020
 */
//Load the route index
const routes = require("./routes");

//Initiallize Route
app.use("/eicu", routes);

/**
 * @description: Unknown Router API calls
 * @author: Rambabu K
 * @date :20-09-2020
 */
app.get("*", function response(req, res) {
  res.send("Entered App js -------");
});
