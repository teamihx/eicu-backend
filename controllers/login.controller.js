const router = require("express").Router();
const userDAO = require("../dao/userDAO");

const loginService = require("../services/loginService");

router.post("/login", function (req, res) {
  //console.log("Login entered------" + JSON.stringify(req.body));
  loginService.login(req, function (data) {
    res.send(data);
  });
});

router.post("/forgotPassword", function (req, res) {
  //console.log("Login entered------" + JSON.stringify(req.body));
  loginService.sendForgotPasswordEmail(req, function (data) {
    res.send(data);
  });
});

router.get("*", (req, callback) => {
  callback.send("This is Login controller");
});

module.exports = router;
