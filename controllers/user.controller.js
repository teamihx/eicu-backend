const router = require("express").Router();
const userService = require("../services/userService");

router.post("/saveUser", function (req, res) {
  //console.log("Login entered------" + JSON.stringify(req.body));
  userService.save(req, function (data) {
    res.send(data);
  });
});

router.get("*", (req, callback) => {
  callback.send("This is Login controller");
});

module.exports = router;
