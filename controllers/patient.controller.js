const router = require("express").Router();
const patientService = require("../services/patientService");
const criticalCareService = require("../services/criticalCare");

router.post("/savePatient", function (req, res) {
  //console.log("Login entered------" + JSON.stringify(req.body));
  patientService.save(req, function (data) {
    res.send(data);
  });
});

/**
 * @description: Create Critical Care
 * @author: Rambabu
 * @date :11-10-2020
 */
router.post("/savecriticalcare", function (req, res) {
  console.log("createcriticalcare entered------" + JSON.stringify(req.body));
  logger.info("createcriticalcare entered------" + JSON.stringify(req.body));
  criticalCareService.create(req, function (data) {
    res.send(data);
  });
});

/**
 * @description: Fetch Critical Care
 * @author: Rambabu
 * @date :17-10-2020
 */
router.get("/retrievecriticalcare/:id", function (req, res) {
  console.log("retrievecriticalcare entered------" + req.params.id);
  logger.info("retrievecriticalcare entered------" + req.params.id);
  criticalCareService.retrieve(req, function (data) {
    res.send(data);
  });
});

router.post("/saveLab", function (req, res) {
  //console.log("saveLab entered------" + JSON.stringify(req.body));
  patientService.saveLab(req, function (data) {
    res.send(data);
  });
});

router.post("/getLab", function (req, res) {
  //console.log("saveLab entered------" + JSON.stringify(req.body));
  patientService.getLabData(req, function (data) {
    res.send(data);
  });
});

router.post("/saveDrug", function (req, res) {
  //console.log("saveLab entered------" + JSON.stringify(req.body));
  patientService.saveDrug(req, function (data) {
    res.send(data);
  });
});

router.post("/getDrug", function (req, res) {
  //console.log("saveLab entered------" + JSON.stringify(req.body));
  patientService.getDrugData(req, function (data) {
    res.send(data);
  });
});

router.post("/stopDrug", function (req, res) {
  //console.log("saveLab entered------" + JSON.stringify(req.body));
  patientService.stopDrup(req, function (data) {
    res.send(data);
  });
});

router.post("/sendMail", function (req, res) {
  //console.log("saveLab entered------" + JSON.stringify(req.body));
  patientService.sendEmailPdf(req, function (data) {
    res.send(data);
  });
});

/**
 * @description: Fetch cc History specifc record based on cc history primary key
 * @author: Rambabu
 * @date :22-10-2020
 */
router.get("/retrieveCCHistory/:id", function (req, res) {
  console.log("retrieveCCHistory entered------" + req.params.id);
  logger.info("retrieveCCHistory entered------" + req.params.id);
  criticalCareService.retrieveSpecificCCHistory(req, function (data) {
    res.send(data);
  });
});

router.get("*", (req, callback) => {
  callback.send("This is patient controller");
});

module.exports = router;
