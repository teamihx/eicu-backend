const router = require("express").Router();

//Hosptal Services
const hospitalService = require("../services/createHospital");

/**
 * @description: Retrieve Hospital controller data
 * @author: Rambabu
 * @date:20-09-2020
 *
 */
router.get("/retrieve", function (req, res) {
  console.log("Hospital controller  is---");
  logger.info("Entered Hospital controller-----");
  hospitalService.retrieve(req, function (data) {
    console.log("Hospital controller response is---" + JSON.stringify(data));
    res.send(data);
  });
});

/**
 * @description: Create Hospital
 * @author: Rambabu
 * @date :20-09-2020
 */
router.post("/create", function (req, res) {
  console.log("Create Hospital entered------" + JSON.stringify(req.body));
  logger.info("Create Hospital entered------" + JSON.stringify(req.body));
  hospitalService.create(req, function (data) {
    res.send(data);
  });
});

/**
 * @description: Fetch Hospital list group by state
 * @author: Rambabu
 * @date :05-10-2020
 */
router.get("/fetchHospital", function (req, res) {
  console.log("Create Hospital entered------" + JSON.stringify(req.body));
  logger.info("Create Hospital entered------" + JSON.stringify(req.body));
  hospitalService.fetchHospital(req, function (data) {
    res.send(data);
  });
});

router.get("/getStateHospitalMenu", function (req, res) {
  console.log("getStateHospitalMenu entered------" + JSON.stringify(req.body));
  logger.info("getStateHospitalMenu entered------" + JSON.stringify(req.body));
  hospitalService.getStateHospital(req, function (data) {
    res.send(data);
  });
});

/**
 * @description:Update Hospital
 * @author: Rambabu
 * @date :02-10-2020
 */
router.put("/update/:id", function (req, res) {
  console.log("update hospital data is----" + req.params.id);
  logger.info("update hospital data is----" + req.params.id);
  hospitalService.update(req, function (data) {
    res.send(data);
  });
});

/**
 * @description: Fetch Bed information along with patient details
 * @author: Rambabu
 * @date :10-10-2020
 */
router.post("/fetchbed", function (req, res) {
  console.log("Fetch bed API entered------" + JSON.stringify(req.body));
  logger.info("Fetch bed API entered-------" + JSON.stringify(req.body));
  hospitalService.fetchbed(req, function (data) {
    res.send(data);
  });
});

router.get("*", (req, callback) => {
  callback.send("This is Hospital controller");
});

module.exports = router;
