const patientModel = require("../models/patient");

exports.update = async (filter, updateReq) => {
  try {
    let resp = await patientModel.findOneAndUpdate(filter, updateReq);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};

exports.save = async (data) => {
  try {
    let patient = await new patientModel(data).save();
    if (patient != null) {
      return patient;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in Save PatientDAO dut to :" + err);
    return null;
  }
};


/**
 * @author:Rambabu.K
 * @date:08-10-2020
 * @description:Fetch Pateinet Details
 * @param {*} pateintid 
 */
exports.getpatientDetails = async (pateintid) => {
  try {
    let result = await patientModel.findOne({
      _id: pateintid,
    });
    if (result) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};