const ccHistory = require("../models/ccHistory");

/**
 * @author:Rambabu.K
 * @date:21-10-2020
 * @description:create critical care  and drug history
 * @param {*} document
 */
exports.create = async (document) => {
  console.log("historyDAO create request is-----" + JSON.stringify(document));
  try {
    let result = await new ccHistory(document).save();
    console.log("historyDAO create result-------" + JSON.stringify(result));
    if (result) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("error in historyDAO creation call----" + err);
    logger.error("error in historyDAO creation call----" + err);
    return null;
  }
};

/**
 * @author:Rambabu.K
 * @date:22-10-2020
 * @description:retrieve critical care history data(id,creadeted_by) based on patient id
 * @param {*} id
 */
exports.retrieve = async (id) => {
  console.log("Entered historyDAO Retrieve-----");
  try {
    //retrieve critical care history based on patient id order by created_date desc
    let result = await ccHistory
      .find({ patient_id: id }, { patient_id: 1, created_date: 1 })
      .sort({ created_date: -1 });
    console.log("historyDAO retrieve result-------" + JSON.stringify(result));
    if (result) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("error in historyDAO  retrieve is----" + err);
    logger.error("error in historyDAO retrieve is----" + err);
    return null;
  }
};

/**
 * @author:Rambabu.K
 * @date:22-10-2020
 * @description:retrieve critical care history based on id primary key
 * @param {*} id
 */
exports.retrievespecficCChistory = async (id) => {
  console.log("Entered historyDAO retrievespecficCChistory-----");
  try {
    //retrieve critical care history based on patient id order by created_date desc
    let result = await ccHistory.findOne({ _id: id });
    console.log(
      "historyDAO retrievespecficCChistory result-------" +
        JSON.stringify(result)
    );
    if (result) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("error in historyDAO retrievespecficCChistory is----" + err);
    logger.error("error in historyDAO retrievespecficCChistory is----" + err);
    return null;
  }
};
