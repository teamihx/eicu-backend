const userModel = require("../models/user");

exports.validateUser = async (userName) => {
  try {
    let user = await userModel.findOne(
      { userName: userName },
      "userName password email isActive firstName lastName"
    );
    if (user) {
      return user;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};

exports.getUserForEmail = async (email) => {
  try {
    let user = await userModel.findOne({ email: email });
    if (user) {
      return user;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};

exports.update = async (filter, updateReq) => {
  try {
    let resp = await userModel.findOneAndUpdate(filter, updateReq);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};

exports.save = async (data) => {
  try {
    let user = await new userModel(data).save();
    if (user != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};
