const hospital = require("../models/hospital");

/**
 * @author:Rambabu.K
 * @date:01-10-2020
 * @description:save hospital data
 * @param {*} document 
 */
exports.create = async (document) => {


    console.log('hopsitalDAO create request is-----' + JSON.stringify(document));
    try {
        let result = await hospital.create(document);
        console.log('hopsitalDAO create hospital result-------' + JSON.stringify(result))
        if (result) {
            return result;
        } else {
            return null;
        }
    } catch (err) {
        console.log('error in hospital DAO hospital creation call----' + err);
        logger.error('error in hospital DAO hospital creation call----' + err);
        return null;
    }
};

/**
 * @author:Rambabu.K
 * @date:01-10-2020
 * @description:retrieve hospital data
 * @param {*} document 
 */
exports.retrieve = async () => {
    console.log('Entered hopsitalDAO Retrieve-----');
    try {
        let result = await hospital.find();
        console.log('hopsitalDAO retrieve hospital result-------' + JSON.stringify(result))
        if (result) {
            return result;
        } else {
            return null;
        }
    } catch (err) {
        console.log('error in hospital DAO hospital fetch is----' + err);
        logger.error('error in hospitalDAO hospital fetch is----' + err);
        return null;
    }
};


/**
 * @author:Rambabu.K
 * @date:02-10-2020
 * @description:update hospital data
 * @param {*} document 
 */
exports.update = async (filter, updateReq) => {
    console.log('hopsitalDAO update id is-----' + filter);
    console.log('hopsitalDAO update request is-----' + JSON.stringify(updateReq));
    try {
        let resp = await hospital.findOneAndUpdate(filter, updateReq);
        if (resp != null) {
            return true;
        } else {
            return false;
        }
    } catch (err) {
        logger.error("Error in hospitalDAO udate call----- " + err);
        return false;
    }
};


/**
 * @author:Rambabu.K
 * @date:06-10-2020
 * @description:fetchHospital hospital data state wise
 * @param {*} document 
 */
exports.fetchHospital = async () => {
    console.log('Entered hopsitalDAO FetchHospital-----');
    try {
       // let result = await hospital.find();
        let result = await hospital.find();
        
       // console.log('hopsitalDAO FetchHospital result-------' + JSON.stringify(result))
        if (result) {
            return result;
        } else {
            return null;
        }
    } catch (err) {
        console.log('error in FetchHospital DAO hospital fetch is----' + err);
        logger.error('error in hospitalDAO FetchHospital fetch is----' + err);
        return null;
    }
};



/**
 * @author:Rambabu.K
 * @date:07-10-2020
 * @description:fetchBed status bed wise
 * @param {*} document 
 */
exports.fetchBedStatus = async (hopital_id,bedno) => {
    console.log('Entered hopsitalDAO fetchBedStatus-----');
    try {
       // let result = await hospital.find();
        let result = await hospital.findOne({ fk_hosp_id: hopital_id,bed_Num:bedno });;
        
        console.log('hopsitalDAO fetchBedStatus result-------' + JSON.stringify(result))
        if (result) {
            return result;
        } else {
            return null;
        }
    } catch (err) {
        console.log('error in FetchHospital DAO hospital fetch is----' + err);
        logger.error('error in hospitalDAO FetchHospital fetch is----' + err);
        return null;
    }
};