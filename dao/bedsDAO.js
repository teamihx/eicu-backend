const bedsModel = require("../models/beds");

exports.update = async (filter, updateReq) => {
  try {
    let resp = await bedsModel.findOneAndUpdate(filter, updateReq);
    if (resp != null) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
};

exports.save = async (data) => {
  try {
    let beds = await new bedsModel(data).save();
    if (beds != null) {
      return beds;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};

exports.getBedForHospitalId = async (hospitalId, bedNum) => {
  try {
    let beds = await bedsModel.findOne({
      fk_hosp_id: hospitalId,
      bed_Num: bedNum,
    });
    if (beds) {
      return beds;
    } else {
      return null;
    }
  } catch (err) {
    return null;
  }
};
