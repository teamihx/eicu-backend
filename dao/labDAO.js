const labModel = require("../models/Lab");

exports.saveLabs = async (data) => {
  try {
    let labRes = await new labModel(data).save();
    if (labRes != null) {
      return labRes;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in Save labDAO dut to :" + err);
    return null;
  }
};

exports.getLabs = async (patient_Id) => {
  try {
    let result = await labModel.find({ patientId: patient_Id });
    if (result != null) {
      return result;
    } else {
      return null;
    }
  } catch (err) {
    console.log("Error occcured in getLabs dut to :" + err);
    return null;
  }
};
