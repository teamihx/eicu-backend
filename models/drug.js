const mongoose = require("mongoose");

const DrugSchema = mongoose.Schema({
  patientId: {
    type: String,
    required: true,
  },
  dateTime: {
    type: Date,
    required: true,
  },
  drugName: {
    type: String,
    required: true,
  },
  prescBy: {
    type: String,
    required: true,
  },
  isStop: {
    type: Boolean,
    required: true,
  },
  stopDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  dose: {
    type: String,
    required: true,
  },
  route: {
    type: String,
    required: false,
  },
  freq: {
    type: String,
    required: false,
  },
  createdBy: {
    type: String,
    required: false,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  modifiedBy: {
    type: String,
    required: false,
  },
  modifiedDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
});

const Drug = mongoose.model("Drug", DrugSchema, "Drug");
module.exports = Drug;
