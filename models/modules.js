const mongoose = require("mongoose");

const ModulesSchema = mongoose.Schema({
  moduleName: {
    type: String,
    required: true,
  },
  moduleCode: {
    type: String,
    required: true,
  },
  isActive: {
    type: Boolean,
  },
});

const Modules = mongoose.model("ModuleMaster", ModulesSchema, "ModuleMaster");
module.exports = Modules;
