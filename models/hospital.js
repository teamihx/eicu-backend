const mongoose = require("mongoose");

const HospitalSchema = mongoose.Schema({
  code: {
    type: String,
    required: false,
  },
  name: {
    type: String,
    required: false,
  },
  email: {
    type: String,
    required: false,
  },
  phone: {
    type: String,
    required: false,
  },
  landLineCode: {
    type: String,
    required: false,
  },
  landLineNum: {
    type: String,
    required: false,
  },
  address: {
    type: String,
    required: false,
  },
  state: {
    type: String,
    required: false,
  },
  country: {
    type: String,
    required: false,
  },
  city: {
    type: String,
    required: false,
  },
  description: {
    type: String,
    required: false,
  },
  isactive: {
    type: Boolean,
    required: false,
  },

  noofbeds: {
    type: Number,
    required: false,
  },
  pincode: {
    type: String,
    required: false,
  },
  bedDetails: [
    {
      bedid: {
        type: Number,
      },
      fk_patient: {
        type: String,
      },
      bedStatus: {
        type: String,
      },
    },
  ],
  crby: {
    type: String,
    required: false,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  upby: {
    type: String,
    required: false,
  },
  modifiedDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
});

const Hospital = mongoose.model("Hospital", HospitalSchema, "Hospital");
module.exports = Hospital;
