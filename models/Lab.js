const mongoose = require("mongoose");

const LabSchema = mongoose.Schema({
  date: {
    type: Date,
    required: false,
  },
  patientId: {
    type: String,
    required: false,
  },
  bloodGroup: {
    type: String,
    required: false,
  },
  haematology: {
    type: String,
    required: false,
  },
  totalLeucocytes: {
    type: String,
    required: false,
  },
  haemoglobin: {
    type: String,
    required: false,
  },
  platelets: {
    type: String,
    required: false,
  },
  neutrophils: {
    type: String,
    required: false,
  },
  lymphocytes: {
    type: String,
    required: false,
  },
  biochemistry: {
    type: String,
    required: false,
  },
  bloodUrea: {
    type: String,
    required: false,
  },
  serCreatinine: {
    type: String,
    required: false,
  },
  totalBilirubin: {
    type: String,
    required: false,
  },
  directBilirubin: {
    type: String,
    required: false,
  },
  indirectBilirubin: {
    type: String,
    required: false,
  },
  totalProtiens: {
    type: String,
    required: false,
  },
  serAlbumin: {
    type: String,
    required: false,
  },
  serGlobulin: {
    type: String,
    required: false,
  },
  sgptAlt: {
    type: String,
    required: false,
  },
  sgptAst: {
    type: String,
    required: false,
  },
  alkPhosphataseAlp: {
    type: String,
    required: false,
  },
  troponinT: {
    type: String,
    required: false,
  },
  troponinI: {
    type: String,
    required: false,
  },
  serumElectrolytes: {
    type: String,
    required: false,
  },
  sodium: {
    type: String,
    required: false,
  },
  potassium: {
    type: String,
    required: false,
  },
  bicarbonate: {
    type: String,
    required: false,
  },
  chlorides: {
    type: String,
    required: false,
  },
  coagulation: {
    type: String,
    required: false,
  },
  bt: {
    type: String,
    required: false,
  },
  ct: {
    type: String,
    required: false,
  },
  pt: {
    type: String,
    required: false,
  },
  inr: {
    type: String,
    required: false,
  },
  aptt: {
    type: String,
    required: false,
  },
  summary: {
    type: String,
    required: false,
  },
  createdBy: {
    type: String,
    required: false,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  modifiedBy: {
    type: String,
    required: false,
  },
  modifiedDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
});

const Lab = mongoose.model("Lab", LabSchema, "Lab");
module.exports = Lab;
