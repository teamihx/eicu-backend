const mongoose = require("mongoose");

const CriticalCareSchema = mongoose.Schema({
    
    ccPid:{
        type: String,
        required: false,
    },

    date: {
        type: Date,
        required: false,
    },
    fk_patient_id: {
        type: String,
        required: false,
      },
    diagnosis: {
        type: String,
        required: false,
    },
    todaysIssues: {
        type: String,
        required: false,
    },
    pendingIssues: {
        type: String,
        required: false,
    },
    clinicalTrends: {
        type: String,
        required: false,
    },
    gcs: {
        type: String,
        required: false,
    },
    pupils: {
        type: String,
        required: false,
    },
    
    oxygenTheraphy: {
        type: String,
        required: false,
    },
    invasive: {
        type: Boolean,
        required: false,
    },
    niv: {
        type: Boolean,
        required: false,
    },
    textAreaMd: {
        type: String,
        required: false,
    },
    temp: {
        type: String,
        required: false,
    },
    hr: {
        type: String,
        required: false,
    },
    bp: {
        type: String,
        required: false,
    },
    vasopressors: {
        type: String,
        required: false,
    },

    ph: {
        type: String,
        required: false,
    },
    pco2: {
        type: String,
        required: false,
    },
    po2: {
        type: String,
        required: false,
    },
    be: {
        type: String,
        required: false,
    },
    hco3: {
        type: String,
        required: false,
    },
    lac: {
        type: String,
        required: false,
    },   

    inputOutput: {
        type: String,
        required: false,
    },

    rrt: {
        type: Boolean,
        required: false,
    },
    
    rrtText: {
        type: String,
        required: false,

    },

    balance: {
        type: String,
        required: false,
    },
    cumBalance: {
        type: String,
        required: false,
    },
    motionPassed: {
        type: Boolean,
        required: false,
    },
    perabdomen: {
        type: String,
        required: false,
    },
    f: {
        type: Boolean,
        required: false,
    },
    a: {
        type: Boolean,
        required: false,
    },
    s: {
        type: Boolean,
        required: false,
    },
    t: {
        type: Boolean,
        required: false,
    },
    h: {
        type: Boolean,
        required: false,
    },
    u: {
        type: Boolean,
        required: false,
    },
    g: {
        type: Boolean,
        required: false,
    },
    textArea1: {
        type: String,
        required: false,
    },
    painScore: {
        type: String,
        required: false,
    },    
    cultureAndAensitivity: {
        type: String,
        required: false,
    },
    antibioticsPerday: {
        type: String,
        required: false,
    },
    invasiveLines: {
        type: String,
        required: false,
    },
    planForTheDay: {
        type: String,
        required: false,
    },
    createdBy: {
        type: String,
        required: false,
    },
    createdDate: {
        type: Date,
        default: Date.now,
        required: false,
    },
    modifiedBy: {
        type: String,
        required: false,
    },
    modifiedDate: {
        type: Date,
        default: Date.now,
        required: false,
    },
});

const CriticalCare = mongoose.model("CriticalCare", CriticalCareSchema, "CriticalCare");
module.exports = CriticalCare;
