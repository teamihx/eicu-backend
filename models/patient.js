const mongoose = require("mongoose");

const PatientSchema = mongoose.Schema({
  bed_id: {
    type: String,
    required: false,
  },
  name: {
    type: String,
    required: true,
  },
  dob: {
    type: Date,
    required: true,
  },
  sex: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  phoneNum: {
    type: String,
    required: true,
  },
  mrNum: {
    type: String,
    required: false,
  },
  dtOfAdmission: {
    type: Date,
    required: true,
  },
  sourOfHistory: {
    type: String,
    required: false,
  },

  prestComplaints: {
    type: String,
    required: false,
  },
  histyOfPrestIllness: {
    type: String,
    required: false,
  },
  medical: {
    type: String,
    required: false,
  },
  dm: {
    type: Boolean,
    required: false,
  },

  ihd: {
    type: Boolean,
    required: false,
  },
  cva: {
    type: Boolean,
    required: false,
  },
  crf: {
    type: Boolean,
    required: false,
  },
  htn: {
    type: Boolean,
    required: false,
  },
  asthma: {
    type: Boolean,
    required: false,
  },
  epilepsy: {
    type: Boolean,
    required: false,
  },
  significantHistory: {
    type: String,
    required: false,
  },
  smoking: {
    type: Boolean,
    required: false,
  },
  alcohol: {
    type: Boolean,
    required: false,
  },
  foodReactions: {
    type: Boolean,
    required: false,
  },
  drugReactions: {
    type: Boolean,
    required: false,
  },
  pulse: {
    type: String,
    required: false,
  },
  bp: {
    type: String,
    required: false,
  },
  resp: {
    type: String,
    required: false,
  },
  temp: {
    type: String,
    required: false,
  },
  spo: {
    type: String,
    required: false,
  },
  wt: {
    type: String,
    required: false,
  },
  respiratory: {
    type: String,
    required: false,
  },
  abdominal: {
    type: String,
    required: false,
  },
  neurology: {
    type: String,
    required: false,
  },
  musculoskeletal: {
    type: String,
    required: false,
  },
  otherSystems: {
    type: String,
    required: false,
  },
  provisionalDiagnosis: {
    type: String,
    required: false,
  },
  planOfCare: {
    type: Array,
    required: false,
  },
  medicalHistory: [
    {
      serialNum: {
        type: String,
        required: true,
      },
      name: {
        type: String,
        required: false,
      },
      dose: {
        type: String,
        required: false,
      },
      dose: {
        type: String,
        required: false,
      },
      route: {
        type: String,
        required: false,
      },
      frq: {
        type: String,
        required: false,
      },
      isreCncl: {
        type: Boolean,
        required: false,
      },
    },
  ],
  createdBy: {
    type: String,
    required: false,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  modifiedBy: {
    type: String,
    required: false,
  },
  modifiedDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  foodDesc: {
    type: String,
    required: false,
  },
  drugDesc: {
    type: String,
    required: false,
  },
});

const Patient = mongoose.model("Patient", PatientSchema, "Patient");
module.exports = Patient;
