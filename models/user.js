const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const UserSchema = mongoose.Schema({
  userName: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  historyOfPassword: {
    type: Array,
    required: false,
  },
  email: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
  isSuperAdmin: {
    type: Boolean,
    required: true,
  },

  isActive: {
    type: Boolean,
    required: false,
    default: true,
  },

  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },

  roleId: {
    type: String,
    required: true,
  },
  createdBy: {
    type: String,
    required: false,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
  modifiedBy: {
    type: String,
    required: false,
  },
  modifiedDate: {
    type: Date,
    default: Date.now,
    required: false,
  },
});
//generate hash
UserSchema.methods.generateHash = (password) => {
  return bcrypt.hashSync(password, 10); //10 salt  rounds
};

UserSchema.methods.isPasswordValid = function (password) {
  return bcrypt.compareSync(password, this.password);
};

const User = mongoose.model("users", UserSchema, "users");
module.exports = User;
