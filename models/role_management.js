const mongoose = require("mongoose");

const roleManagementSchema = mongoose.Schema({
  roleName: {
    type: String,
    required: true,
  },
  modules: [
    {
      _id: {
        type: String,
        required: true,
      },
      moduleName: {
        type: Boolean,
        required: false,
      },
    },
  ],
  isActive: {
    type: Boolean,
    required: true,
  },
});

const roleManagement = mongoose.model("role_managements", roleManagementSchema);
module.exports = roleManagement;
