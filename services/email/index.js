const nodemailer = require("nodemailer");

exports.sendMail = async (to, subject, text, attachments) => {
  /*const transporter = nodemailer.createTransport({
    host: "smtpout.secureserver.net",
    secure: true,
    secureConnection: false, // TLS requires secureConnection to be false
    tls: {
      ciphers: "SSLv3",
    },
    requireTLS: true,
    port: 465,
    debug: true,
    auth: {
      user: process.env.EMAIL_AUTH_USER,
      pass: process.env.EMAIL_AUTH_PSWD,
    },
  });*/

  var transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "eiculast009@gmail.com",
      pass: "Test@123",
    },
  });

  /* const mailOptions = {
    from: process.env.EMAIL_AUTH_USER,
    to,
    subject,
    html: text,
    attachments,
  };*/

  var mailOptions = {
    from: "eiculast009@gmail.com",
    to,
    subject,
    html: text,
    attachments,
  };

  try {
    let info = await transporter.sendMail(mailOptions);
    console.log(
      "Email sent: " + info.response + " :  to " + to + " : subject " + subject
    );
    return {
      status: "SUCCESS",
      message: "Email Sent",
    };
  } catch (err) {
    console.log(err);
    return {
      status: "FAILED",
      message: "Email failed to send, Please try again",
    };
  }
};

module.exports = exports;
