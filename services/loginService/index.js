/**
 * @description: This Service class will do Hospital  CRUD operations
 * @author: Rambabu
 * @date :20-09-2020
 */

const JSON = require("circular-json");
const { isNotNull, isArrayNotEmpty } = require("../../utils/validators");
const { responseCreator } = require("../../utils/responsehandler");
const randomstring = require("randomstring");
const userDAO = require("../../dao/userDAO");
const emailService = require("../email");
const forgotPasswordEmail = require("../../emailTemplates/forgotPasswordEmail");
const template = require("../../emailTemplates/template");

var LoginService = {
  /**
   * @package services\branch
   * @param {*} query
   * @param {*} callback
   * @author: Rambabu
   * @date:2020
   * @description: Branch Creation
   */
  login: async function (req, callback) {
    let final_result = null;
    console.log("Entered login create  service" + JSON.stringify(req.body));
    try {
      let user = await userDAO.validateUser(req.body.userName);
      console.log("user login" + JSON.stringify(user));
      if (user) {
        if (!user.isActive) {
          final_result = responseCreator(
            null,
            200,
            false,
            "User disabled. Please contact Customer Support Team."
          );
        }
        let validPwd = await user.isPasswordValid(req.body.password);
        if (validPwd === true) {
          final_result = responseCreator(user, 200, true, null);
        } else {
          final_result = responseCreator(
            null,
            200,
            false,
            "Invalid username and password"
          );
        }
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Invalid username and password"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in login due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Invalid username and password"
      );
      callback(final_result);
    }
  },

  sendForgotPasswordEmail: async function (req, callback) {
    console.log(
      "Entered sendForgotPasswordEmail create  service" +
        JSON.stringify(req.body)
    );
    let final_result = null;

    let emailData = {};
    let token = randomstring.generate(7);
    console.log("Token " + JSON.stringify(token));
    emailData.password = token;

    let user = await userDAO.getUserForEmail(req.body.email);
    console.log("User data " + JSON.stringify(user));
    user.password = user.generateHash(emailData.password);
    let updateReq = {
      password: user.password,
      modifiedDate: new Date(),
    };
    const filter = { _id: user._id.toString() };
    let resp = await userDAO.update(filter, updateReq);
    console.log("Response " + JSON.stringify(resp));
    if (resp) {
      const html = await forgotPasswordEmail.forgotPasswordEmail(emailData);
      const templates = await template.template(html);

      await emailService.sendMail(req.body.email, "Forgot Password", templates);
      final_result = responseCreator(
        null,
        200,
        true,
        "send email successfully"
      );
    } else {
      final_result = responseCreator(null, 200, true, "send email failed");
    }

    callback(final_result);
  },
};

module.exports = LoginService;
