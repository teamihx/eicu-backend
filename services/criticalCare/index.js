/**
 * @description: This Service class will do Hospital  CriticalCare operations
 * @author: Rambabu
 * @date :11-10-2020
 */

const JSON = require("circular-json");
const { isNotNull, isArrayNotEmpty } = require("../../utils/validators");
const { responseCreator } = require("../../utils/responsehandler");
const criticalCareDAO = require("../../dao/criticalCareDAO");
var criticalCare = require("../../models/criticalCare");
const drugDAO = require("../../dao/drugDAO");
var drug = require("../../models/drug");
const historyDAO = require("../../dao/historyDAO");

var CriticalcareService = {
  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:11-10-2020
   * @description: CriticalCare Creation
   */
  create: async function (req, callback) {
    console.log(
      "Entered CriticalCare create  service" + JSON.stringify(req.body)
    );
    try {
      //if in critical care id is not avaliable below code will execute
      if (!isNotNull(req.body.ccPid)) {
        //if critical care id not avaliable need to insert data
        let cc_res = await criticalCareDAO.create(req.body);

        //get drug details based on patient id
        let drug_result = await drugDAO.getDrugs(req.body.fk_patient_id);

        //constructing criticalcare and drug json data for history saving
        let cc_care = [];
        let drug_ch = [];
        cc_care.push(req.body);
        drug_ch.push(drug_result);

        const obj = {
          patient_id: req.body.fk_patient_id,
          created_date: new Date(),
          created_by: "test@gmail.com",
          critical_care: cc_care,
          drug_chart: drug_ch,
        };
        //save drug json and critical care result in istory collection
        let history_res = await historyDAO.create(obj);

        console.log(
          "result in create criticalcare api" + JSON.stringify(cc_res)
        );

        if (isNotNull(cc_res)) {
          callback(responseCreator("Saved Successfully...", 200, true, null));
        } else {
          callback(
            responseCreator("Not saved successfully...", 500, false, null)
          );
        }
      } else if (isNotNull(req.body.ccPid)) {
        /* here critical care id is not null then we have to update data into critical care 
      and create new record in history collection */
        //if critical care id avaliable need to update data

        console.log("======= ID ==== :" + req.body.ccPid);
        let cc_res = await criticalCareDAO.update(req.body.ccPid, req.body);

        //get drug details based on patient id
        let drug_result = await drugDAO.getDrugs(req.body.fk_patient_id);
        console.log("test 2 : " + JSON.stringify(req.body.fk_patient_id));
        console.log("test 3 : " + JSON.stringify(drug_result));

        //constructing criticalcare and drug json data for history
        let cc_care = [];
        let drug_ch = [];
        cc_care.push(req.body);
        drug_ch.push(drug_result);

        const obj = {
          patient_id: req.body.fk_patient_id,
          created_date: new Date(),
          created_by: "test@gmail.com",
          critical_care: cc_care,
          drug_chart: drug_result,
        };
        console.log("save object " + JSON.stringify(obj));
        //save drug json and critical care result in istory collection
        let history_res = await historyDAO.create(obj);

        console.log(
          "result in criticalcare create api" + JSON.stringify(cc_res)
        );

        if (isNotNull(cc_res)) {
          callback(responseCreator("Saved Successfully...", 200, true, null));
        } else {
          callback(
            responseCreator("Not saved successfully...", 500, false, null)
          );
        }
      }
    } catch (err) {
      console.log(
        "err occured in criticalcare create index js due to : " + err
      );
      logger.error(
        "err occured in criticalcare create index js due to : " + err
      );
      callback(responseCreator(null, 500, false, err));
    }
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:17-10-2020
   * @description: CriticalCare Retrieve
   */
  retrieve: async function (req, callback) {
    console.log("Entered CriticalCare create  service" + req.params.id);
    try {
      //below code fetch critical care data based on patient id
      let result = await criticalCareDAO.retrieve(req.params.id);

      //below code fetch cc hisory based on patient id
      let cc_hisitory_result = await historyDAO.retrieve(req.params.id);

      const obj = {
        cc_results: result,
        cc_history: cc_hisitory_result,
      };

      console.log("result in create hospital api-----" + JSON.stringify(obj));

      if (isNotNull(obj)) {
        callback(responseCreator(obj, 200, true, null));
      } else {
        callback(responseCreator("Data not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log("err occured in create hospital index js due to : " + err);
      logger.error("err occured in create hospital index js due to : " + err);
      callback(responseCreator(null, 500, false, err));
    }
  },

  /**
   * @param {*} req
   * @param {*} callback
   * @author: Rambabu
   * @date:22-10-2020
   * @description: retrieveSpecificCCHistory  based on cchistory primary key
   */
  retrieveSpecificCCHistory: async function (req, callback) {
    console.log("Entered retrieveSpecificCCHistory  service" + req.params.id);
    try {
      //below code fetch critical care history data based on cc history primary key
      let result = await historyDAO.retrievespecficCChistory(req.params.id);

      console.log(
        "result in retrieveSpecificCCHistory api-----" + JSON.stringify(result)
      );
      if (isNotNull(result)) {
        callback(responseCreator(result, 200, true, null));
      } else {
        callback(responseCreator("Data not avaliable...", 500, false, null));
      }
    } catch (err) {
      console.log(
        "err occured in retrieveSpecificCCHistory index js due to : " + err
      );
      logger.error(
        "err occured in retrieveSpecificCCHistory index js due to : " + err
      );
      callback(responseCreator(null, 500, false, err));
    }
  },
};

module.exports = CriticalcareService;
