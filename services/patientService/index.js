/**
 * @description: This Service class will do Hospital  CRUD operations
 * @author: Rambabu
 * @date :20-09-2020
 */

const JSON = require("circular-json");
const { responseCreator } = require("../../utils/responsehandler");
const patientDAO = require("../../dao/patientDAO");
const bedsDAO = require("../../dao/bedsDAO");
const labDAO = require("../../dao/labDAO");
const bcrypt = require("bcrypt");
const moment = require("moment");
const drugDAO = require("../../dao/drugDAO");
const emailService = require("../email");
const testEmail = require("../../emailTemplates/testEmail");
const template = require("../../emailTemplates/template");
const pdf = require("html-pdf");

var PatientService = {
  /**
   * @package services\branch
   * @param {*} query
   * @param {*} callback
   * @author: Rambabu
   * @date:2020
   * @description: Branch Creation
   */
  save: async function (req, callback) {
    let final_result = null;
    console.log("Entered save create  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      let bedsRes = null;
      bedsRes = await bedsDAO.getBedForHospitalId(
        data.hospital_id,
        data.bedNum
      );
      if (bedsRes) {
        //ignore
      } else {
        let bed = {};
        bed.bed_Num = data.bedNum;
        bed.fk_hosp_id = data.hosp_id;
        bed.status = "vacant";
        bed.createdBy = data.usrid;
        bed.createdDate = new Date();
        bed.modifiedBy = data.usrid;
        bed.modifiedDate = new Date();
        bedsRes = await bedsDAO.save(bed);
      }

      if (bedsRes) {
        data.bed_id = bedsRes._id.toString();
        data.dob = moment.utc(data.dob + "T00:00:00Z").toDate();
        data.dateOfAdmission = moment
          .utc(data.dateOfAdmission + "T00:00:00Z")
          .toDate();
        data.createdBy = data.usrid;
        data.createdDate = new Date();
        data.modifiedBy = data.usrid;
        data.modifiedDate = new Date();
        let patient = await patientDAO.save(data);
        console.log("Patient saved : " + JSON.stringify(patient));
        if (patient) {
          const filter = { _id: bedsRes._id.toString() };
          let bedUpdate = await bedsDAO.update(filter, {
            fk_patient_id: patient._id.toString(),
            status: "occupy",
          });
          final_result = responseCreator(patient, 200, true, null);
        } else {
          final_result = responseCreator(
            null,
            200,
            false,
            "Unable to create patient"
          );
        }
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to create patient"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in saving user due to : " + err);
      final_result = responseCreator(null, 200, false, "Unable to create User");
      callback(final_result);
    }
  },
  saveLab: async function (req, callback) {
    let final_result = null;
    console.log("Entered save create  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      let responsedata = [];
      for (let lab of data.labdata) {
        lab.createdDate = new Date();
        lab.modifiedDate = new Date();
        lab.modifiedBy = lab.createdBy;
        lab.date = moment.utc(lab.date).toDate();
        let labRes = await labDAO.saveLabs(lab);
        if (labRes) {
          responsedata.push(labRes);
        }
      }
      console.log("saveLab == " + JSON.stringify(responsedata));

      if (responsedata) {
        final_result = responseCreator(responsedata, 200, true, null);
      } else {
        final_result = responseCreator(null, 200, false, "Unable to save Labs");
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in saveLab due to : " + err);
      final_result = responseCreator(null, 200, false, "Unable to save Labs");
      callback(final_result);
    }
  },
  getLabData: async function (req, callback) {
    let final_result = null;
    console.log("Entered getLabData  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      let responsedata = [];
      let results = await labDAO.getLabs(data.patientId);

      console.log("getLab == " + JSON.stringify(results));

      if (results) {
        final_result = responseCreator(results, 200, true, null);
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to get Lab data"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in getLabData due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Unable to get Lab data"
      );
      callback(final_result);
    }
  },
  saveDrug: async function (req, callback) {
    let final_result = null;
    console.log("Entered saveDrug create  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      let responsedata = [];
      for (let drug of data.drugdata) {
        drug.createdDate = new Date();
        drug.modifiedDate = new Date();
        drug.createdBy = data.usrid;
        drug.modifiedBy = data.usrid;
        drug.dateTime = moment.utc(drug.dateTime).toDate();
        drug.patientId = data.patientId;
        if (drug.isStop) {
          drug.stopDate = new Date();
        }
        let drugRes = await drugDAO.saveDrug(drug);
        if (drugRes) {
          responsedata.push(drugRes);
        }
      }
      console.log("drugRes == " + JSON.stringify(responsedata));

      if (responsedata) {
        final_result = responseCreator(responsedata, 200, true, null);
      } else {
        final_result = responseCreator(null, 200, false, "Unable to save Labs");
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in saveDrug due to : " + err);
      final_result = responseCreator(null, 200, false, "Unable to save Drug");
      callback(final_result);
    }
  },
  getDrugData: async function (req, callback) {
    let final_result = null;
    console.log("Entered getDrugData  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      let responsedata = [];
      let results = await drugDAO.getDrugs(data.patientId);

      console.log("getDrugData == " + JSON.stringify(results));

      if (results && results.length > 0) {
        final_result = responseCreator(results, 200, true, null);
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to get Drug data"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in getDrugData due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Unable to get Drug data"
      );
      callback(final_result);
    }
  },
  sendEmailPdf: async function (req, callback) {
    let final_result = null;
    console.log("Entered getDrugData  service" + JSON.stringify(req.body));
    try {
      const html = await testEmail.testEmailsTemp({ test: "nme" });
      const templates = await template.template(html);
      // const PDF = await emailService.createPDF(templates, {});
      pdf.create(templates, {}).toBuffer((err, buffer) => {
        if (err) {
          return console.log("error");
        }
        emailService
          .sendMail(
            req.body.email,
            "Test Eamil Log",
            "Please find your requested attachment",
            [
              {
                filename: "Test.pdf",
                content: buffer,
              },
            ]
          )
          .then((sendEmailRes) => {
            if (sendEmailRes.status === "success") {
              final_result = responseCreator("", 200, true, null);
              callback(final_result);
            } else {
              final_result = responseCreator("", 200, true, null);
              callback(final_result);
            }
          });
      });
      let attachemen = [
        {
          filename: "filename.pdf",
          content: Buffer.from(templates, "utf-8"),
          contentType: "blob",
        },
      ];

      final_result = responseCreator("", 200, true, null);
      callback(final_result);
    } catch (err) {
      console.log("err occured in getDrugData due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Unable to get Drug data"
      );
      callback(final_result);
    }
  },
  stopDrup: async function (req, callback) {
    let final_result = null;
    console.log("Entered stopDrup  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      let responsedata = [];
      const filter = { _id: data._id.toString() };
      data.isStop = true;
      data.stopDate = new Date();
      data.modifiedDate = new Date();
      data.modifiedBy = data.usrid;
      let results = await drugDAO.update(filter, data);

      console.log("getDrugData == " + JSON.stringify(results));

      if (results && results.length > 0) {
        final_result = responseCreator(results, 200, true, null);
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to get Drug data"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in getDrugData due to : " + err);
      final_result = responseCreator(
        null,
        200,
        false,
        "Unable to get Drug data"
      );
      callback(final_result);
    }
  },
};

module.exports = PatientService;
