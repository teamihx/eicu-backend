/**
 * @description: This Service class will do Hospital  CRUD operations
 * @author: Rambabu
 * @date :20-09-2020
 */

const JSON = require("circular-json");
const { isNotNull, isArrayNotEmpty } = require("../../utils/validators");
const { responseCreator } = require("../../utils/responsehandler");
const userDAO = require("../../dao/userDAO");
const bcrypt = require("bcrypt");

var UserService = {
  /**
   * @package services\branch
   * @param {*} query
   * @param {*} callback
   * @author: Rambabu
   * @date:2020
   * @description: Branch Creation
   */
  save: async function (req, callback) {
    let final_result = null;
    console.log("Entered save create  service" + JSON.stringify(req.body));
    try {
      let data = req.body;
      data.createdBy = "";
      data.createdDate = new Date();
      data.modifiedBy = "";
      data.modifiedDate = new Date();
      data.password = bcrypt.hashSync(data.password, 10);
      let user = await userDAO.save(data);
      console.log("user login" + JSON.stringify(user));
      if (user) {
        final_result = responseCreator(user, 200, true, null);
      } else {
        final_result = responseCreator(
          null,
          200,
          false,
          "Unable to create User"
        );
      }

      callback(final_result);
    } catch (err) {
      console.log("err occured in saving user due to : " + err);
      final_result = responseCreator(null, 200, false, "Unable to create User");
      callback(final_result);
    }
  },
};

module.exports = UserService;
